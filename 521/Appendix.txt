Add just before last line of NIPS file i.e: \end{document}

\section{Appendix}

We attempt classification through a tree-based model approach called CART (Classification and Regression Trees).  In this method, we use the rpart() function from �rpart� package to classify the crime categories. As before, the dependent variable is the crime-category and the independent variables are resolution, location and time related information. We train the model using the top 10 categories for the year of 2014 . We achieve a misclassification error rate of 53 \% and a root node error of 61 \% with this approach. The variables used in prediction were �Resolution� and �PdDistrict�. We therefore decide to focus on categories that change with time and location. Based on the random forest model above, we focus on 6 categories viz. prostitution, larceny/theft, drug/narcotic, robbery. With the new categories, the misclassification error rate reduces to 13 \% and the error at the root node reduces to 20 \% with �Latitude�, �Longitude� and �Resolution� variables picked by the model for tree construction.. The tree plots after both the methods are presented below. 
However, this method does not work without the �Resolution� variable which we do not have any information about when we are trying to predict if a particular category of crime takes place. However, knowing the probability of resolution in a particular district might help us predict the crime better.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{tree.png}
\caption{Tree with top-10 categories}
\label{default}
\end{center}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{tree_new.png}
\caption{Tree with 6 specific categories}
\label{default}
\end{center}
\end{figure}